# Words With Their Definitions

Asymetric Ciphers - The use of related keys, (Private keys and public keys) They are generated with these things called "Cryptographic Algorithms" which are a type of encryption algorithm that are mainly used to encrypt data.

Authentication - A way of confirming a persons online identity. For example, there are sometimes 2 factor authentication, which normally sends an email to the recipient and they further verify and login from there.

Bias - A way of pushing your belief in favor of some idea or thing, that sometimes could be unfair.

Certificate Authority - It is a thing that stores issued online certificates. It pretty much classifies that someone has the ownership of a specific certificate.

Citizen Science - Scientific research that has some participation from anyone. 

Creative Commons Licensing - Mainly known as "cc" for copywriting something that you had created, and is used for the person to share their idea and only certain people. 

Crowdsourcing - A group of peoples services put together to put something big together, so for example, a non-profit organization packing food for ukraine.

Cybersecurity - The protection of computer systems from malicous people who might steal information from you.

Data Mining - The process of finding out patterns in data sets out of statistics, data base systems, or machine learning. 

Decryption - When data is encrypted, decryption brings the data to its original form. 

Digital Divide - The unequal access for digital tech, which could include things like phones, tablets, laptops, or just internet connection. It is pretty much just a wall for some people to reach information online.

Encryption - A way for a blob of something gets turned into random characters, that can be decrypted later. This could be used to send text messages to another person.

Intellectual Property - IP is the creations of the mind, like inventions, or paintings, that are used in commerce.

Keylogging - The action of recording actions, like keys being typed on a keyboard. Pretty much just your actions being monitored.

Malware - Software intentionally deisgned to cause some malicous activity on a computer system, which gives unwanted access to the people who caused the attack to things like your bank account.

Multifactor Authentication - A more secured way of protecting something, using multiple steps to verify themselves.

Open Access - Something that is free of charge, or any type of thing blocking you from using it. 

Open Source - Some piece of software that is released under a persons license, which pretty much gives rights to the person who created it to use it, and distribute it.

Free Software - Computer software that is distributed under certain terms that allows the user to run it for any reason.

FOSS - Free and Open-Sourced Software, which is a term for free software and any open source software where anyone is able to use it.

Personal Identifiable Information - Any representation of info that permits a persons identity who the information applies to be inferred by direct or indirect means.

Phishing - A form of social engineering that are made by hackers to decieve people to get them to reveal information they wouldnt normally give out.

Plagiarism - The act of stealing someone elses work.

Public Key Encryption - The field of cryptography that use pairs of keys. (Public and private keys again) and keys are generated with algorithms.

Rogue Access Point - Something that has been installed on a secure network without any autorization.

Targeted Marketing - The use of advertisements to get you to do somethign based off of some internet data.

Virus - A fast spreading piece of malware that goes onto your computer that can do harm to it.
