# CPT Scoring Guidelines


**Row 1: Program Purpose and Function** Shows input, output, clearly explains program functionality. Must not differ from the actual function of the program.

**Row 2: Data Abstraction** Includes 2 program code statements ONE: that shows the data has been stored in a list TWO: Showing that the data is the same list that is fulfilling the codes purpose.

**Row 3: Managing Complexity** Includes a segment that manages complexity in the program. Explains how the list is used for that purpose. Explains how you could've done the same without it. The list should not be irrelevent to the program. It must be nescessary.

**Row 4: Procedural Abstraction** Program must include a student written procedure and call it. The description should mention what it does, its purpose, and each of its use cases.

**Row 5: Algorithm Implementation**: The written response must describe a procedure that is called twice. Each call must have a different argument passed. It must also the conditions that are tested with each call. Finally must identify the result of the calls. Make sure the two calls make the code do different things. If they do the same thing, you won't be awarded a point. Also the calls have to *not* be, in their own words: ""

**Row 6: Testing** Shows that the selected procudure must be called twice, each with a different argument. It also must show that the procudure returns a result.

**AP Glossary** Input, Program Functionality, Output, Purpose, Program Code Segment, List, Data Has Been Stored In This List, Collection Type, List Being Used, Student-Developed Procedure, Procedure, Parameters, Algorithm, Squenching, Selection, Iteration, and Arguements. 
