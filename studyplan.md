The APCSP exam is scored by 70% is multiple choice, and the other 30% is
something called a performance task.

For a lot of AP exams, the threshold for getting a 5 is getting a 100 to around 95, then for a 4 you need to get a 94, to a 85, and for a 3 you need to get a 84to around 75 on the AP exam.

The Create Performance Task is a creation of a computer program of the students choice, you need at least 12 hours of in class work time to work on it. It is scored as the other 30% of your score for the full 100% the AP exam has.

I scored best on Big Idea 1 Creative Development, which I got a 45.45% (5/11) and the one I need more improvement on is Big Idea 4 Computer Systems and Networks which I only got a 1/6 or a 16.66%. 

I will use Codecademy to study for the test, because they have a lot of AP computer science courses that could help me get a better grade on the exam. Edx has some programming help for the AP course. It can help in almost all the fields of the big ideas. There is also a website called mreliot.com which has free APCS Principles lessons, which could also help with the studying for the ap Exam. 
