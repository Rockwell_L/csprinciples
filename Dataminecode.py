import time
import statistics
answers = ["A", "B", "C", "D"]
qna = [[1], [1], [1], [1], [1]]
openit = open("questions.dat", "r")
readit = openit.read()
questions = readit.splitlines()


def Question1(answers, questions, qna, j):
  q1 = input(questions[0])
  for i in range(4):
    if q1.upper() != answers[i]:
      i = i+1
    else:
      qna[0][j] = q1
      return qna
  print("Invalid answer")
  Question1(answers, questions, qna, j)


def Question2(answers, questions, qna, j):
  q2 = input(questions[1])
  for i in range(4):
    if q2.upper() != answers[i]:
       i = i+1
    else:
      qna[1][j] = q2
      return qna
  print("Invalid answer")
  Question2(answers, questions, qna, j)
    

def Question3(answers, questions, qna, j):  
  q3 = input(questions[2])
  for i in range(4):
    if q3.upper() != answers[i]:
       i = i+1
    else:
      qna[2][j] = q3
      return qna
  print("Invalid answer")
  Question3(answers, questions, qna, j)

  
def Question4(answers, questions, qna, j):
  q4 = input(questions[3])
  for i in range(4):
    if q4.upper() != answers[i]:
       i = i+1
    else:
      qna[3][j] = q4
      return qna
  print("Invalid answer")
  Question4(answers, questions, qna, j)


def Question5(answers, questions, qna, j):    
  q5 = input(questions[4])
  for i in range(4):
    if q5.upper() != answers[i]:
       i = i + 1
    else:
      qna[4][j-1] = q5
    return qna
  print("Invalid answer")
  Question5(answers, questions, qna, j)


def ShowSelections(qna, j): 
  print("Here are your responses: ")
  time.sleep(1)
  print("For question number 1 you selected: ")
  time.sleep(1)
  print(qna[0][j])
  time.sleep(1)
  print("For question number 2 you selected: ")
  time.sleep(1)
  print(qna[1][j])
  time.sleep(1)
  print("For question number 3 you selected: ")
  time.sleep(1)
  print(qna[2][j])
  time.sleep(1)
  print("For question number 4 you selected: ")
  time.sleep(1)
  print(qna[3][j])
  time.sleep(1)
  print("For question number 5 you selected: ")
  time.sleep(1)
  print(qna[4][j])


def averageans(qna, j):
  print("Finding average answers...")
  time.sleep(1)
  print("Calcuating most common replies...")
  time.sleep(5)
  print("The average answer for Question 1 was:")
  if len(qna[0])!= 2:
    print(statistics.mode(qna[0]))
  else:
    print(qna[0])
  print("The average answer for Question 2 was: ")
  if len(qna[1])!= 2:
    print(statistics.mode(qna[1]))
  else:
    print(qna[1])
  print("The average answer for Question 3 was: ")
  if len(qna[2])!= 2:
    print(statistics.mode(qna[2]))
  else:
    print(qna[2])
  print("The average answer for Question 4 was: ")
  if len(qna[3])!= 2:
    print(statistics.mode(qna[3]))
  else:
    print(qna[3])
  print("The average answer for Question 5 was: ")
  if len(qna[4])!= 2:
    print(statistics.mode(qna[4]))
  else:
    print(qna[4])


print("This is the survey. You will be asked questions and your answers will recorded and displayed as new information depending on what you selected at the end. You are then asked to start. To start type yes (It doesn't need to be capitalized). You will then go through the questions. When you are done, it will state that you're done.  When you are done, your answers will be analyzed and displayed as new information")
numFriends = int(input("How many people will be taking the quiz? "))
for j in range(numFriends):
  choicestart = input("Start? ")
  if choicestart.capitalize() == "Yes":
    time.sleep(1)
    num1 = Question1(answers, questions, qna, j)
    qna[0].append(1)
    time.sleep(1)
    num2 = Question2(answers, questions, qna, j)
    qna[1].append(1)
    time.sleep(1)
    num3 = Question3(answers, questions, qna, j)
    qna[2].append(1)
    time.sleep(1)
    num4 = Question4(answers, questions, qna, j)
    qna[3].append(1)
    time.sleep(1)
    num5 = Question5(answers, questions, qna, j)
    qna[4].append(1)
    print("Analyzing...")
    time.sleep(3)
    ShowSelections(qna, j)
  else: 
    print("Invalid input, if you want to start, type yes")

averageans(qna, j)

