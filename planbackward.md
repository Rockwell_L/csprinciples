# 3A
Our input would be things like ' x = input("Hello") and our output would be determined based on what they enter, so if there was a prompt for starting, if they say start, it would start.

# 3B
I would use a list for the inventory system, for example, each item in the game would have a value linked to a number inside of the list.

# 3C
Yes it does, it has sequencing because it cycles through commands to take turns in battle. It does have selection, because of things like changing weapons.

# 3D
Yes, because we have a lot of different if statements, because they could be doing a lot, like inputing a lot, etc.
