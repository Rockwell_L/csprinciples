# Repeating Steps
for loops are used for repeating code
range is used for creating a set or list of numbers (for i in range...)

Loops make it easier to do simple tasks, as it takes faster and you dont have torepeat the same line of code over, and over again.

Computers know how to repeat steps. They do it by using a loop or iteration.

## Repeating with Numbers
A for loop is one of the types of ways to repeat a bunch of statements in a program.
The for loop will take a variable and make it look through a set of values on a list. A list kind of holds a bunch of values, in a set order. 

Indents are pretty much things that the code isnt against the wall on the left, or the boundary. It also follows up the ":" and adds to the previous code (if _ then:...)

There is also something called a body, which is the thing that is repeating in a loop.

### What is a List?
A list is something inside of [] so for example, [1, 2, 3] they are seperated by commas and spaces, and lists are very useful in many circumstances. A list has a specific order that the programmer sets it in.

#### Range Function
Range function loop is a way to loop over a set of numbers
When range functions are called with a single positive integeer the code will run and will end up generating ALL the values of the integers from 0 to the number, subtracting one. 

If two integer values are inputed instead, them it will generate each of the values from the first value, but then it would go to one less than the second value. 

##### There's a Pattern Here!
There are lots of patterns in programs, which is common when processing data. The patterns are called the "Accumulator Pattern" 

There are 5 steps to the Accumulator Pattern,
1. You set the accumulator variable to its initial value, and that isnt the value we want if there is no data to be processed.
2. Get ALL of the data processed.
3. You step through all data by using a for loop so that the variable takes on all the values in the data
4. Combine the pieces of the data into the accumulator
5. You do something with the result.

###### Adding Print Statements
Print calls to display the results of the program, after using print(), inside the parenthesis is the thing you want to print, when using text, you add quotations (""), of course with the print in front of the parenthesis.
