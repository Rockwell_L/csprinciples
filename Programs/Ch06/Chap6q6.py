def move(t, f, r):
    t.forward(f)
    t.right(r)
    t.forward(f)
    t.right(90)
    t.forward(r)
    t.right(f)
    t.forward(r)
    t.right(f)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)
