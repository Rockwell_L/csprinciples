def trip(total):
    miles = 500
    miles_per_gallon = 26
    price_per_gallon = 3.45
    num_gallons = miles / miles_per_gallon
    total = num_gallons * price_per_gallon
    return total

total = trip
print(trip(total))


