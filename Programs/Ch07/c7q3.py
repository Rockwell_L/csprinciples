def sum_list(num_list):
     total =  0
     for n in num_list:
         total = total + n
     return total

my_nums = [0, 1, 2, 3, 4, 5]
print(sum_list(my_nums))
