import random
import time


def RerunQ1():
  print("Invalid input! ")
  Question1()

def RerunQ2():
  print("Invalid input! ")
  Question2()

def RerunQ3():
  print("Invalid input! ")
  Question3()

def RerunQ4():
  print("Invalid input! ")
  Question4()

def RerunQ5():
  print("Invalid input! ")
  Question5()

def Question1():
  q1 = input("What is the best color: A, Green; B, Blue;\nC, Yellow; or D, Red? ")
  
  if q1.capitalize() == ["A","B","C","D"]:
    return q1  
  else:
    RerunQ1()

def Question2():
  q2 = input("Which food is superior: A, Pizza; B, Sandwhich; C, Burger; or D, French Fries? ")
  
  if q2.capitalize() == ["A","B","C","D"]:
    return q2  
  else:
    RerunQ2()

def Question3():
  q3 = input("What is the best season: A, Summer; B, Spring; C, Fall; or D, Winter? ")
  
  if q3.capitalize() == ["A","B","C","D"]:
    return q3  
  else:
    RerunQ3()  

def Question4():
  q4 = input("What is the best drink: A, Milk; B, Water; C, Soda; or D, Orange Juice? ") 
  
  if q4.capitalize() == ["A","B","C","D"]:
    return q4  
  else:
    RerunQ4()

def Question5():
  q5 = input("What is the condiment: A, Ketchup; B, Mustard; C, Relish; or D, Mayonnaise? ")
  
  if q5.capitalize() == ["A","B","C","D"]:
    return q5  
  else:
    RerunQ5()


print("This is the survey, you do this with your friends and with one computer. You will be asked questions and you answers will recorded and eventually you will learn the average answers you gave during the survey. To start you will be asked how many people will be participating in the survey. You are then asked to start. To start type yes(It doesn't need to be capitalized). You will then go through the questions. When you will be done it will say that you're done. You will then be prompted to start again. Pass the computer to you friend and then they will answer. When you are all done, the average answers will be shown.")
numPlayers = int(input("How many people will be taking the survey? "))
do = True
while do == True:
  choicestart = input("Start? ")
  if choicestart.capitalize() == "Yes":
    print("Starting...")
    time.sleep(3)
    Question1()
    time.sleep(3)
    Question2()
    time.sleep(3)
    Question3()
    time.sleep(3)
    Question4()
    time.sleep(3)
    Question5()
    time.sleep(3)
  else: 
    print("Invalid input, if you want to start, type yes")
  do = False

