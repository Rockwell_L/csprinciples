# Types of loops + intro
Loops are something that repeats a body of code, a number of times. The body of the loop is what follows the for statment and are indented to the right of it.(4 spaces = 1 indent)

For loop - A type of loop that normally runs until it doesn't need to be run anymore, for example, when x reaches 5, the loop will end. 

While loop - Repeats code until a logical expression is true. Logical expressions are things that are true or false, for example, things like z > 8 or x < 3.

## Infinite Loops
Infinite loops are types of loops that will never end, pretty much straight forward. The code tests if a statement is true, and if that statement is always true, it will keep running the body code. For example, while 5 == 5: which is true, will stay true and keep running what is in the body of the loop.

### Counting with a While Loop
Easy for a computer to repeat something a specific number of times.
You can have a "counter" variable inside the loop that will count each time the body runs. So if you want to stop once that counting variable reaches 10, you make it so the loop is while counter < 11 (is less than 11) it will continue to run, making it so you can run the body 10 times.

#### Looping When We Don't Know When We'll Stop
This is the thing, the body wont stop running until the loop's conditionals are met. If you want to find a square root, you can't always be exact. So, you use this method of finding the square root of a number.
Start by guessing 2.

Compute the guess squared.

Is the guess squared close to the target number? If it’s within 0.01, you are done.

If its not close enough, you divide the target number by the guess, then average that number with the guess.

Thats then the new guess that you use, then you square it and go back to step 3.

##### Nasted For Loops
Bodies of loops can hold another loop. So you can run a program a certain number of times that can run another program another set of times, but it would only happen if the first loop is still running.  
