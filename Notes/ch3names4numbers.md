#Chapter 3 Notes

##Assigning a Name
A variable can change during a game.
It is called a variable because it varies.
You can also set a name with the current value, naming the variable.

A variable can be a box that has a  label on it
The value can be anything that can be represented on a computer, and it can be
stored in the memory.
Setting a variables value is called an assignment  
If a = 18 then 18 is the value of the variable
There are restrictions for variable names
Must start with an uppercase or lowercase letter (or underscore)
Can contain digits ( Text1, Text2 )
Cannot be a python keyword 
A variable names result is not the same as a variable named Result
    
### Expressions
The right side of the assignment does not have to be a set value
modulo is the remainder which the symbol for it is %

#### Chicago to Dallas
The problems I just did were called Tracing a program.
The function print can take an input whose value will be displayed.
It will print the exact contents of the string.

