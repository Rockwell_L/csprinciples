# Notes

You have a more than 74% chance of getting a pass on the AP exam on average.
The test is split up 70% multiple choice questions, and 30% is the CPT.
There are around 65 questions on the test give or take.
You can be graded with a 1,2,3,4, or 5.
The college board website tells you a lot about the test, since they are the people making the tests. 
There are multiple samples on the college board website, which gives sample videos from past years and theur written response to get a 5 from that part of the test.
Also on the college board wensite, there is a scoring guidelines tab that you can go to, to see how you can get a 5.
W3School has tutorials for different languages to help study for the CPT. 


# Question Practice


### Source

https://www.test-guide.com/quiz/ap-comp-sci-1.html 

## Question 1:

You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database.

However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. 

- [ ] Option 1: Rerunning the code
- [x] Option 2: Code Tracing
- [ ] Option 3: Using a code visulaizer
- [ ] Option 4: Using DISPLAY statements at different points of code

## Question 2:

Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?

- [ ] Option 1: The file was moved from the original location, causing some information to be lost
- [ ] Option 2: The recording was done through the lossless compression technique.
- [ ] Option 3: The song file was saved with higher bits per second
- [x] Option 4: The song file was saved with lower bits per second

## Question 3:


A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students.

Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [x] Option 1: Email the prospective families that have middle-school-age children.
- [ ] Option 2: Create a report based on the student body's overall performance for a marketing pamphlet.
- [ ] Option 3: Post an interactive pie chart about the topics and scores of the passing students on the schools' website
- [ ] Option 4: Post the results of the passing students on social media sites.
