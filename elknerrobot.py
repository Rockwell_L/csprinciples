from gasp import *
import random

def collided(thing1, list_of_things):
    for thing2 in list_of_things:
        if thing1.x == thing2.x and thing1.y == thing2.y:
            return True
    return False



class Player:
    def __init__(self):
        self.x = random.randint(0, 63)
        self.y = random.randint(0, 47)
        while collided(self,  robots):
            self.x = random.randint(0, 63)
            self.y = random.randint(0, 47)
        self.shape =  Circle((10*self.x+5, 10*self.y+5), 5, filled=True)

    def move(self):
        key = update_when("key_pressed")
        can_move = {
            'left': self.x > 0,
            'right': self.x < 63,
            'up': self.y < 47,
            'down': self.y > 0
        }
        keys = {
            'teleport': ['KP_5', 's', 'k'],
            'down_left': ['KP_1', 'z', 'm'],
            'down': ['KP_2', 'x', 'comma'],
            'down_right': ['KP_3', 'c', 'period'],
            'left': ["KP_4", "a", "j"]
        }

        while key in keys['teleport']: 
            remove_from_screen(self.shape)
            self.x = random.randint(0, 63)
            self.y = random.randint(0, 47)
            self.shape = Circle((10*self.x+5, 10*self.y+5), 5, filled=True)
            key = update_when("key_pressed")

        if key in keys['down_left']: 
            if can_move['down']:
                self.x -= 1
            if can_move['left']:
                self.y -= 1
        elif key in keys['down'] and can_move['down']:
            self.y -= 1
        elif key in keys['down_right']:
            if can_move['right']:
                self.x += 1
            if can_move['down']:
                self.y -= 1
        elif key in keys['left'] and can_move['left']:
            self.x -= 1
        elif key in ["KP_6", "d", "l"] and self.x < 63:
            self.x += 1
        elif key in ["KP_7", "q", "u"]:
            if self.x > 0:
                self.x -= 1
            if self.y < 47:
                self.y += 1
        elif key in ["KP_8", "w", "i"] and self.y < 47:
            self.y += 1
        elif key in ["KP_9", "r", "o"]:
            if self.x < 63:
                self.x += 1
            if self.y < 47:
                self.y += 1

        move_to(self.shape, (10*self.x+5, 10*self.y+5))

class Robot:
    def __init__(self):
        self.x = random.randint(0, 63)
        self.y = random.randint(0, 47)
        self.junk = False
        self.shape = Box((10*self.x, 10*self.y),10, 10)



"""

def safely_place_player():
    global player

    place_player()

    while collided(player, robots):
        place_player()

    player.shape = Circle((10*player.x+5, 10*player.y+5), 5, filled=True)


def place_robots(numbots):
    global robots

    robots = []

    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0, 63)
        robot.y = random.randint(0, 47)
        robot.junk = False
        robot.shape = Box((10*robot.x, 10*robot.y), 10, 10)
        robots.append(robot)


def move_player():
    global player

    key = update_when("key_pressed")

    while key in ["KP_5", "s", "k"]:
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when("key_pressed")

    if key in "1zm":
        if player.x > 0:
            player.x -= 1
        if player.y > 0:
            player.y -= 1
    elif key in ["KP_2", "x", "comma"] and player.y > 0:
        player.y -= 1
    elif key in ["KP_3", "c", "period"]:
        if player.x < 63:
            player.x += 1
        if player.y > 0:
            player.y -= 1
    elif key in ["KP_4", "a", "j"] and player.x > 0:
        player.x -= 1
    elif key in ["KP_6", "d", "l"] and player.x < 63:
        player.x += 1
    elif key in ["KP_7", "q", "u"]:
        if player.x > 0:
            player.x -= 1
        if player.y < 47:
            player.y += 1
    elif key in ["KP_8", "w", "i"] and player.y < 47:
        player.y += 1
    elif key in ["KP_9", "r", "o"]:
        if player.x < 63:
            player.x += 1
        if player.y < 47:
            player.y += 1

    move_to(player.shape, (10*player.x+5, 10*player.y+5))


def move_robots():
    global robots

    for robot in robots:
        if not robot.junk:
            if robot.x > player.x:
                robot.x -= 1
            elif robot.x < player.x:
                robot.x += 1

            if robot.y > player.y:
                robot.y -= 1
            elif robot.y < player.y:
                robot.y += 1

            move_to(robot.shape, (10 * robot.x, 10 * robot.y))


def collided(thing1, list_of_things):
    for thing2 in list_of_things:
        if thing1.x == thing2.x and thing1.y == thing2.y:
            return True
    return False


def robot_crashed(the_bot):
    for a_bot in robots:
        if a_bot == the_bot:
            return False
        if a_bot.x == the_bot.x and a_bot.y == the_bot.y:
            return a_bot
    return False


def check_collisions():
    global finished, robots

    # Handle player crashes into robot
    if collided(player, robots):
        finished = True
        Text("You've been caught!", (320, 240), size=36)
        sleep(3)
        return

    # Handle robots crashing into each other
    surviving_robots = []

    for robot in robots:
        if collided(robot, junk):
            continue

        jbot = robot_crashed(robot)

        if not jbot:
            surviving_robots.append(robot)
        else:
            remove_from_screen(jbot.shape)
            jbot.junk = True
            jbot.shape = Box(
                (10 * jbot.x, 10 * jbot.y), 10, 10, filled=True
            )
            junk.append(jbot)

    robots = []

    for robot in surviving_robots:
        if not collided(robot, junk):
            robots.append(robot)

    if not robots:
        finished = True
        Text("You win!", (160, 240), size=36)
        sleep(3)
        return



	game_over = False
numbots = 4

while not game_over:
    finished = False
    junk = []

    place_robots(numbots)
    safely_place_player()

    while not finished:
        move_player()
        move_robots()
        check_collisions()

    if numbots < 17:
        numbots *= 2
        clear_screen()
    else:
        game_over = True

"""
begin_graphics()
numbots = 8
robots = [Robot() for i in range (numbots)]
player = Player()

while True:
    player.move()

end_graphics()
