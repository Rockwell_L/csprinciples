# Terms
Bandwidth - The maximum speed data can transfer across a path.

Computing device - A machine that can perform computations by using logic and other things, without any human assistance.

Computing network - Different computer networks that can exchange data with each other.

Computing system - One or more computers that share storage.

Data stream - Process of sending a continuous data flow to a thing of software.

Distributed computing system - A system that has components on other computers which get data by sending messages between them.

Fault-tolerant - A thing that some systems can do that can still process suff even if something fails

Hypertext Transfer Protocol (HTTP) - Application that allows people to communicate stuff on the web.

Hypertext Transfer Protocol Secure (HTTPS) - Pretty much just HTTP, but more secure.

Internet Protocol (IP) address - A bunch of numbers that identifies a device on a network.

Packets - Data that is a small part of a bigger message.

Parallel computing system - A type of computing where multiple calculations occur at once.

Protocols - A set of rules for processing things.

Redundancy - A system that duplicates its components just in case it fails.

Router - A computer that sends data packets throughout different networks.

## Chapter 5 Notes

The Domain Name System (DNS) is a hierarchical naming system used to convert human-readable domain names into machine-readable IP addresses. It enables users to access websites, servers, and other internet resources using easy-to-remember names instead of IP addresses, which are difficult to remember. DNS allows servers to be moved to a new network connection without affecting the end-users' ability to access the server.

The DNS comprises a distributed database and a set of protocols that enable clients to access the database. The DNS database is organized as a hierarchical tree structure, with the root at the top, followed by top-level domains, second-level domains, and so on. The root of the DNS tree is managed by the Internet Assigned Numbers Authority (IANA), and the top-level domains are managed by various organizations designated by the Internet Corporation for Assigned Names and Numbers (ICANN).

When a user types a domain name into a browser, the browser sends a DNS query to a DNS resolver, which looks up the IP address associated with that domain name in the DNS database. The resolver then returns the IP address to the browser, which uses it to establish a connection to the server hosting the requested resource.


