# Notes Chap1

In the 1900s, the way the communicated using calls was through a microphone and a bunch of wires connected to speakers. 
The phone back then worked much better the closer you were to an operator's building and a wire could be direcly brought to that persons house, for a direct and clean connection.
The number of wires that a telephone pole had, was enough for one long distance phone call each. (At a time)
Messages for computers can take a while to download or process, or sometimes they take not so long.
If you had enough patience, you could send a message a long ways away by using network connections.
The idea of breaking down messages into packets was made in the 1960's. 
It wasn't used until the 1980's.
Routers made it possible to do multiple "hops" 
Each computer was given a number or address to classify it.
Packets are parts of a larger message.

# Notes Chap2

Engineers engineer and build complex systems such as the internet by breaking them into smaller problems, which are the link, internetwork, and the transport and application layers. The link layer deals with connection and the application layer is what users interact with. Routers move packets across multiple links to get them to the destination, and the Transport layer deals with lost packets. The application layer defines how the two halves of the application exchange messages.

# Notes Chap3
The Link layer is the lowest layer of internet architecture, closest to the physical network media, and transmits data using wires, fiber optic cables, or radio signals. WiFi is an example of a link layer and uses a unique MAC address for each device. To coordinate the multiple radios, Carrier Sense Multiple Access with Collision Detection is used, as well as a token approach when data needs to be sent efficiently.

# Notes Chap4
Getting an IP address is important for computers to be able to communicate in the network. The IP address is broken into two parts, the network number and the Host Identifier. Routers use the Network Number to determine the best outbound link for the packet, while traceroute can be used to trace the approximate route a packet takes. IP addresses can also be used to detect and recover from routing loops.

# Notes Chap5
The Domain Name System allows users to access websites by their domain name instead of IP addresses. ICANN assigns top level domains like .com, .edu, and .org to other organizations which can then assign subdomains within their domain. Also, ICANN assigns two letter country code top level domain names to countries. This system allows servers to be moved to a new location without affecting the user's ability to access it.

# Notes Chap6
The Transport layer is responsible for reliability, message reassembly and retransmission. It adds a header to each packet with information about the source and destination, as well as a time to live field. It also uses window size to adjust how much data is sent at once. Client and server applications communicate through ports, and there are well known ports for common applications like FTP and HTTP.

# Notes Chap7
Writing networked applications requires a protocol like HTTP or IMAP to define rules for conversations between two applications. Flow control is used to ensure data is sent at the appropriate speed. Telnet can be used to explore the HTTP protocol. To write applications, a programming language library is needed to send and receive data.

