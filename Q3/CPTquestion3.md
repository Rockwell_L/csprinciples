##1:Describes the overall purpose of the program
The overall purpose was to teach us how to use gasp.
##2:Describes what functionality of the program is demonstrated in the video.
It shows the robots colliding with each other.
##3:Describes the input and output of the program demonstrated in the video.
The input would probably be the motions, like WASD or something like that. The outputs would be the actual movement of the character after a key is pressed.

##1:The first program code segment must show how data have been stored in the list.
![](/Users/1006795/Desktop/Screenshots\ and\ images/Screen \ Shot 2022-12-07\ at\ 5.42.31\ PM.png)
##2:The second program code segment must show the data in the same list being used, such as creating new data from the existing data or accessing multiple elements in the list, as part of fulfilling the program’s purpose.
![](/Users/1006795/Desktop/Screen\ Shot\ 2022-12-07\ at\ 5.46.00\ PM.png)


##1:The first program code segment must be a student-developed procedure that
##Defines the procedure’s name and return type (if necessary)
##Contains and uses one or more parameters that have an effect on the functionality of the procedure
##Implements an algorithm that includes sequencing, selection, and iteration
![]()
##2: The second program code segment must show where your student-developed procedure is being called in your program.
![]()
##3:Describes in general what the identified procedure does and how it contributes to the overall functionality of the program
![]()
##4:Explains in detailed steps how the algorithm implemented in the identified procedure works. Your explanation must be detailed enough for someone else to recreate it.
![]()
##1:Describes two calls to the procedure identified in written response 3c. Each call must pass a different argument(s) that causes a different segment of code in the algorithm to execute.
###First call:
![]()
###Second call:
![]()
##2:Describes what condition(s) is being tested by each call to the procedure
###Condition(s) tested by the first call:
![]()
###Condition(s) tested by the second call:
![]()
##3:Identifies the result of each call
###Result of the first call:
![]()
###Result of the second call:
![]()


