### Criteria
1. The quiz question with answer choices, including which quiz and which item it is.
2. State your wrong answer and why you chose it.
3. State the correct answer and explain why it is correct.

# Quiz 1

Question 1: A video-streaming Web site uses 32-bit integers to count the number of times each video has been played. In anticipation of some videos being played more times than can be represented with 32 bits, the Web site is planning to change to 64-bit integers for the counters. Which of the following bed describes the result of using 64-bit integers instead of 32-bit integers?

- [ ] a. 2 times as many values can be represented.
- [ ] b. 32 times as many values can be represented.
- [x] c. 2^32 times as many values can be represented.
- [ ] d. 32^2 times as many values can be represented.

Why I chose B originally, was because I forgot that when doing these types of problems, you use exponents per bits, meaning that 32 was incorrect.

Why C is correct, is because 2^32 because it is twice the amount 32 bits use (2^16) 

Question 3:The circuit below shows a circuit composed of two logic gates. The output of the circuit is true.

- [x] a. Input A must be true.
- [ ] b. Input A must be false.
- [ ] c. Input A can either be true or false.
- [ ] d. There is no possible value of input A that will cause the circuit to have the output true.

Why I chose C, was because I didn't look at the bottom of the logic table, and didn't realize the output was True.

Why A is correct, is because the statement ends up as True, not false.

Question 7: Every night at 12:00 am, you notice that some files are being uploaded from you PC device to an unfamiliar website without your knowledge. You check the file to find that these files are from the day's work at the office. What is this an example of?

- [ ] a. Phising attack
- [ ] b. Keylogger Attack
- [x] c. Computer Virus
- [ ] d. Malware insertion point

Why I chose B, was because I forgot that keylogger attacks are tracking what people are doing, like key strokes.

Why C is correct is because its a program that is stealing information off of the computer that is infected.

Question 10: An organization needs to process large amounts of data that cannot be handled by the current PC setup and has extra PC devices laying aroujnd in the storage room. What sort of conection should this organization consider to improve its processing capabilities?

- [ ] a. The organization should consider maintaining their sequential computing model.
- [x] b. The organization should consider using a distributed computing model.
- [ ] c. The organization should consider limiting the data colletction requirements.
- [ ] d. The organization should consider adding additional servers.

Why I chose C, was because I thought it would help spread out the data between all computers while still keeping that computer good.

Why B is correct, is because this will do what I thought before, distributing the data throughout all the computers, but I didn't realize this was here.

# Quiz 2

Question 9: Using binary search, how many iterations would it take to find the letter W?

- [ ] a. 2
- [x] b. 3
- [ ] c. 23
- [ ] d. 24

Why I chose C was because I forgot to split, which made me get it incorrect.

Why B is correct is because thats what you get when you split the thing.

# Quiz 3

Question 3: Digital alarm clocks display information and visual indicators to help people wake up on time. Which of the indicators could represent a single bit of information? Select two answers.

- [ ] a. The current month (1-12)
- [x] b. The "PM/AM" indicator
- [ ] c. The current hour (1-12)
- [x] d. The temperature unit indicator ("C" or "F")

Why I chose C & D, was because D is correct, but I forgot that a number is more than 1 bit of information.

Why B & D is correct, is because C or F is able to be represented by 1 bit, along with the PM/AM indicator.

# Quiz 4

Question 1: What can replace <.missing condition> to complete the last column of the table?

- [ ] a. A and B
- [ ] b. A or B
- [ ] c. Not (A or B)
- [x] d. A and not(B)

Why I chose C was because I didn't see the connection between A or B and the missing condition.

Why D is correct is because all A conditions are true.

Question 2: A researcher is analyzing data about students in a school distric to determine whether there is a relationship between grade point average and number of absences. The researcher plans on compiling data from several sources to create a record for each student. The researcher has access to a database with the following information about each student:

* Last Name
* First Name
* Grade Level (9, 10, 11, or 12)
* Grade point average (on a 0.0 to 4.0 scale)

The researcher also has access to another database with the following information about each student.

* First Name
* Last Name
* Number of absences from school
* Number of late arrivals to school

Upon compiling the data, the researcher identifies a problem due to the fact that neither data source uses a unique ID number for each student. Which of the following best describes the problem caused by the lack of unique ID numbers?

- [x] a. Students how have the same name may be confused with each other.
- [ ] b. Students who has the same grade point average may be confused with each other.
- [ ] c. Students who have the same grade level may be confused with each other,
- [ ] d. Students who have the same number of absences may be confused with each other.

Why I chose B was because I made a careless error.

Why A is correct is because it doesn't specify what parts of the name, its the whole name that would be the same.

Question 3: A team of researchers wants to create a program to analyze the amount of pollution reported in roughly 3,000 counties across the US. The program is intended to combine county data sets and then process the data. Which of the following is most likely to be a challenge in creating the program?

- [ ] a. A computer program cannot combine data from different files.
- [x] b. Different counties may organize data in different ways.
- [ ] c. The number of counties is too large for the program to process.
- [ ] d. The total number of rows of data is too large for the program to process.

Why I chose D was because I thought that they wouldnt have good enough computers to handle data from 3,000 counties.

Why B is correct is because not all 3,000 counties probably use the same way to organize their data. 

Question 4: A student is creating a Web site that is intended to display information about a city based on a city name that a user enters in a text field. Which of the following are likely to be challenges associated with processing city names that usera might provide as input? Select two answers.

- [ ] a. Users mught attempt to use the Web site to search for multiple cities.
- [x] b. Users might enter abbreviations for the names of the cities.
- [x] c. Users might misspell the name of the city.
- [ ] d. Users might be slow at typing the city name in the text field.

Why I chose A and C was because C was right, but I chose A because it might be hard to find cities when they do multiple idk.

Why B and C are right is because mispelling the cities can mess up the database's results, and that the database may not take abbreviations for the cities.

Question 5: A database of information about shows at a concert venue contains the following information.

* Name of artist performing at the show
* Date of show
* Total dollar amount of all tickets sold

Which of the following additional pieces of information would be most useful in determining the artist with the greatest attendance during a particular month?

- [x] a. Average ticket price
- [ ] b. Length of the show in minutes
- [ ] c. Start time of the show
- [ ] d. Total dollar amount of food and drinks sold during the show

Why I chose D was because I thought that it would be the most useful because it would tell you who would want to spend the most money on it and therefore seeing how much money would have been made.

The reason why A is right and D is wrong is because D doesn't tell you which had the most people because it could have sold more expensive things, and A tells you the average of everything which could help you calculate how many people saw the show.

Question 8: Consider the following procedure called mystery, which is intended to display the number of times a number target appears in a list. Which of the following best describes the behavior of the procedure? Select two answers. 

- [x] a. The program correctly displays the count if the target is not in the list.
- [ ] b. The program never correctly displays the correct value for count.
- [x] c. The program correctly displays the count if the target appears once in the list and also at the end of the list.
- [ ] d. The program always correctly displays the correct value for count.

I chose A and D, and D was wrong because it won't always display the correct value for the count variable because it could mess up what is in list due to target.

The correct answers are A and C because it will correctly show it because the target is wrong, because the code is wrong, and c is correct because if it appears once, which meants if it goes through the loop once it will display correcly.

Question 9: The diagram below shows a circuit composed of logic gates. Each gate takes two inputs and produces a single output. What will the algorithm produce?

- [ ] a. True when both A and B are true only
- [ ] b. True when A is True and B is false
- [x] c. Always true regardless of the values of A and B
- [ ] d. Always false regardless of the values of A and B

I originally chose B because I made another careless error and didn't realize that this doesn't have 1 false statement in it,

It is C because it will always be True due to no false statements appearing in the table.

Question 10: If your algorithm needs to search through a list of unsorted words, what type of search would you use?

- [x] a. Linear search
- [ ] b. Binary search
- [ ] c. Bubble sort
- [ ] d. Insertion sort

I originally chose C, because I forgot what this was

The correct answer is A because Linear search can search for unsorted wordsInsertion sort

I originally chose C, because I forgot what this was

The correct answer is A because Linear search can search for unsorted words.

# THIS TOOK ME SO LONG TO DO (I HAND WROTE EVERYTHING)
