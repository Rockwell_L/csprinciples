# Terms and The Definitions

Abstraction - When you simplify the code but make the code cleaner and more efficient.

Analog Data - Data that is shown physically.

Bias - Repeated errors that a computer makes that give unfair responses.

Binary Number System - A table of numbers where only 2 values are possible for each digit, a 0, or a 1.

Bit - The smallest piece of data on a system/computer.

Byte - Its a piece of data that has 8 binar digits within it.

Classifying Data - The process of moving data around into certain categories that makes it easier to use.

Cleaning Data - The process of working on removing bad data, from a bunch of data.

Digital Data - The form of data that computers can understand.

Filtering Data - Looking through small parts of data and analyzing it.

Information - Stuff that is recorded about anything.

Lossless Data Compression - A type of data compression that doesnt lose any data in the compressing process.

Lossy Data Comression - After compression, the file is not brought back to the original form of the file.

Metadata - Metadata is a type of data that gives information out about data.

Overflow Error - An overflow error occurs when data is too big for the data type.

Patterns in Data - A bunch of data that repeats.

Round-Off or Rounding Error - When the computer responds with the exact number response to something that infinitely goes on, for example, 1/3 is 0.33 repeated, but its 0.333333333 (that goes on forever) 

Scalability - A way to measure a systems capabilities to change the performance.
