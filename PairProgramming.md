Pair Programming is a type of programming where you work on coding with another person. It goes between the driver and the obvserver or the navigator. These 2 people can go back and forth between roles.
These 2 people make a strong team, since they can help each other bug check, its pretty much just 2 minds one program, making it easier to write. 

![](PairProgramming.png)

Main reasons why pair programming is cool:
1. Before code is written, the pair discusses and plans out the program, rather than just start typing away on a specific topic.
2. Two people are working on this program, so they can work on it on and off. If one person wants to work on it at night, they can. Even if the other person is sleeping. This just makes it easier and the other people can check and change any bugs the other person had written without them knowing.
3. You can learn off of the other person. This can help develop your coding skills, and make their bug senses tingle whenever they look at code because of the fact that they checked their partners code. Partner coding can substantially make your solo development coding so much better.

The roles in Pair Programming
1. The driver. The driver writes the code, making the ideas the pair made earlier, a reality.
2. The Navigator. The navigator checks the code, to check for any errors. This makes sure there are no bugs in the code, and makes it so the code runs well.
3. These 2 jobs can be switched between back and forth.

