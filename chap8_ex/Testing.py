def whats_this(num_list):
    """
      >>> whats_this([1, 2, 3, 4])
      2.5
      >>> whats_this([1, 2, 3, 4, 5])
      3
      >>> whats_this([9, 8, 5, 11, 2])
      5
      >>> whats_this([9, 8, 10, 2])
      9.0
      >>> whats_this([0, 2, 4, 6, 8, 10, 12])
      6
      >>> whats_this([0, 2, 4, 6, 8, 10, 12, 14])
      7.0
    """
    middle_index = len(num_list) // 2
    if len(num_list) % 2 == 0:
        return (num_list[middle_index - 1] + num_list[middle_index]) / 2
    return num_list[middle_index]

if __name__ == "__main__":
    import doctest
    doctest.testmod()
