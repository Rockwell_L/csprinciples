def sum_func(start, stop):
    sum = 0
    num = 0
    while num in range(start, stop + 1):
        sum = sum + num
    return sum

print(sum_func(1, 10))


